FROM shimmi/jenkins:latest

RUN wget https://download.java.net/java/GA/jdk11/13/GPL/openjdk-11.0.1_linux-x64_bin.tar.gz
RUN tar -xzvf openjdk-11.0.1_linux-x64_bin.tar.gz
RUN export JAVA_HOME=$(pwd)/jdk-11.0.1